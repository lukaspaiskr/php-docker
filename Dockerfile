FROM php:5.6-apache

RUN docker-php-ext-install mysql mysqli

RUN apt-get update -y

RUN apt-get update && \
    apt-get install -y libfreetype6-dev libjpeg62-turbo-dev zlib1g-dev libpng-dev && \
                    docker-php-ext-install mysqli && \
                    docker-php-ext-install mbstring && \
                    docker-php-ext-configure gd --with-freetype-dir=/usr/include/ --with-jpeg-dir=/usr/include/  &&  \
                    docker-php-ext-install gd

# xdebug
RUN pecl install xdebug && \
    echo "zend_extension=/usr/local/lib/php/extensions/no-debug-non-zts-20131226/xdebug.so\n[xdebug]\nxdebug.remote_enable = 1\nxdebug.remote_connect_back = 1" > /usr/local/etc/php/conf.d/ext-xdebug.ini

RUN docker-php-ext-install zip


EXPOSE 80

COPY . /var/www/html
WORKDIR /var/www/html