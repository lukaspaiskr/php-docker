<?php
ini_set('display_errors', 1);
/**
 * Created by PhpStorm.
 * User: lukasskywalker
 * Date: 22.11.17
 * Time: 11:06
 */
$rImg = imagecreatefrompng( "twms.png" );
$color = imagecolorallocate($rImg, 0, 0, 0);
//825x339
$text = urldecode($_GET['text']);
$length = strlen($text)*10;
//imagestring( $rImg,10,750-$length,300,$text, $color);
$font = 'MPro.ttf';
imagettftext($rImg, 15, 0, 750-$length,300, $color, $font, $text);
header('Content-type: image/png');
imagepng($rImg);